/*
 * @Author: ylyu
 * @Date: 2024-02-23 16:19:17
 * @LastEditors: ylyu
 * @LastEditTime: 2024-02-23 16:48:01
 * @Description:
 */
const { app, ipcMain } = require('electron')
const appLogout = () => {
  ipcMain.handle('on-logout-event', (e, arg) => {
    console.log('您已退出应用程序！')
    // 异步操作，退出程序
    app.quit() // 显示调用quit才会退出
    app.exit()
    return 'success'
  })
}

module.exports = appLogout
