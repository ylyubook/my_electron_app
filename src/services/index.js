/*
 * @Author: ylyu
 * @Date: 2024-02-22 14:02:45
 * @LastEditors: ylyu
 * @LastEditTime: 2024-02-24 21:09:41
 * @Description:
 */

import { AppPost, AppGet, AppPost2 } from '@/utils/request'

// 登录
const checkLogin = (keyword) => {
  return AppPost('/login', {
    ...keyword,
  })
}
// 抖音用户 /system/douyinuser/list
const getUserList = (keyword) => {
  return AppPost('/system/douyinuser/list', {
    keyword,
  })
}

// 用户初始化
const initNewUser = (keyword) => {
  return AppPost('/system/douyinuser/add', keyword)
}

// 创建用户
const createNewUser = (keyword) => {
  return AppPost('/dyWebDriver/createWebdriver', keyword)
}

// 删除用户
const removeUser = (keyword) => {
  return AppPost('/system/douyinuser/remove', keyword)
}

// 获取全局配置信息
const getConfig = (keyword) => {
  return AppGet('/dyWebDriver/getDyConfig', {
    keyword,
  })
}
// /dyWebDriver/updateDyConfig
const updateConfig = (keyword) => {
  return AppPost('/dyWebDriver/updateDyConfig', keyword)
}

// 场控列表options
const getCKList = (keyword) => {
  return AppPost('/system/ck/list', {
    keyword,
  })
}

// 客服列表options
const getKFList = (keyword) => {
  return AppPost('/system/kf/list', {
    keyword,
  })
}

// 关闭/停止
const closeDriver = (keyword) => {
  return AppPost2('/dyWebDriver/closeWebDriver', keyword)
}

// 启动
const startDriver = (keyword) => {
  return AppGet('/dyWebDriver/getWebDriver', keyword)
}

// 秒杀下架
const seckillAll = (keyword) => {
  return AppPost2('/dyWebDriver/seckillALl', keyword)
}

// 智能回复列表数据
const getKFData = (keyword) => {
  return AppPost('/system/kfxx/list', {
    keyword,
  })
}
// getCKData
const getCKData = (keyword) => {
  return AppPost('/system/ckxx/list', {
    keyword,
  })
}

export {
  checkLogin,
  getUserList,
  initNewUser,
  createNewUser,
  removeUser,
  getConfig,
  updateConfig,
  getCKList,
  getKFList,
  startDriver,
  closeDriver,
  seckillAll,
  getKFData,
  getCKData,
}
// module.exports = { checkLogin, getUserList }
