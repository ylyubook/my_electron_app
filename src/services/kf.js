/*
 * @Author: ylyu
 * @Date: 2024-02-22 14:02:45
 * @LastEditors: ylyu
 * @LastEditTime: 2024-02-24 20:44:56
 * @Description:
 */

import { AppPost, AppGet, AppPost2 } from '@/utils/request'
// 查询
const getKFData = (keyword) => {
  return AppPost('/system/kfxx/list', {
    keyword,
  })
}
// 客服列表options
const getKFList = (keyword) => {
  return AppPost('/system/kf/list', {
    keyword,
  })
}
// 新增
const addKFData = (keyword) => {
  return AppPost('/system/kfxx/add', keyword)
}

// 修改
const updateKFData = (keyword) => {
  return AppPost('/system/kfxx/edit', keyword)
}

// 删除
const removeKFData = (keyword) => {
  return AppPost('/system/kfxx/remove', keyword)
}

export { getKFData, getKFList, addKFData, updateKFData, removeKFData }
