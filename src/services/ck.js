/*
 * @Author: ylyu
 * @Date: 2024-02-22 14:02:45
 * @LastEditors: ylyu
 * @LastEditTime: 2024-02-24 19:09:03
 * @Description:
 */

import { AppPost, AppGet, AppPost2 } from '@/utils/request'
// 查询
const getCKData = (keyword) => {
  return AppPost('/system/ckxx/list', {
    keyword,
  })
}
// 场控列表options
const getCKList = (keyword) => {
  return AppPost('/system/ck/list', {
    keyword,
  })
}
// 新增
const addCKData = (keyword) => {
  return AppPost('/system/ckxx/add', keyword)
}

// 修改ok
const updateCKData = (keyword) => {
  return AppPost('/system/ckxx/edit', keyword)
}

// 删除
const removeCKData = (keyword) => {
  return AppPost('/system/ckxx/remove', keyword)
}

export { getCKData, getCKList, addCKData, updateCKData, removeCKData }
