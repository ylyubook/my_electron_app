/*
 * @Author: ylyu
 * @Date: 2024-02-22 13:54:43
 * @LastEditors: ylyu
 * @LastEditTime: 2024-02-23 14:53:33
 * @Description:
 */
// 根据当前mode获取Base Url 类型
const env = process.env.VUE_APP_BASE_URL_TYPE

// 根据VUE_APP_BASE_URL_TYPE获取BASE_URL
const getBaseUrl = (env) => {
  switch (env) {
    // development
    case 'dev':
      return {
        baseUrl: 'http://localhost:8899',
      }
    // production
    case 'prod':
      return {
        baseUrl: 'http://10.10.11.167:8095',
      }
    // default: dev
    default:
      return {
        baseUrl: 'http://localhost:8899',
      }
  }
}

const appConfig = {
  baseUrl: getBaseUrl(env).baseUrl,
}

export default appConfig
