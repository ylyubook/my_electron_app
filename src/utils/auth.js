/*
 * @Author: ylyu
 * @Date: 2024-02-22 15:45:22
 * @LastEditors: ylyu
 * @LastEditTime: 2024-02-22 15:46:51
 * @Description:
 */
// localstorage

//定义参数 类型 window.localStorage,window.sessionStorage,
const config = {
  type: 'localStorage', // 本地存储类型 localStorage/sessionStorage
  prefix: 'SDF_0.0.1', // 名称前缀 建议：项目名 + 项目版本
  expire: 1, //过期时间 单位：秒
  isEncrypt: true, // 默认加密 为了调试方便, 开发过程中可以不加密
}
// 设置 setStorage
export const setStorage = (key, value, expire = 0) => {
  if (value === '' || value === null || value === undefined) {
    value = null
  }

  if (isNaN(expire) || expire < 1) throw new Error('Expire must be a number')

  expire = (expire ? expire : config.expire) * 60000
  let data = {
    value: value, // 存储值
    time: Date.now(), //存值时间戳
    expire: expire, // 过期时间
  }

  window[config.type].setItem(key, JSON.stringify(data))
}

// 获取 getStorage
export const getStorage = (key) => {
  // key 不存在判断
  if (
    !window[config.type].getItem(key) ||
    JSON.stringify(window[config.type].getItem(key)) === 'null'
  ) {
    return null
  }

  // 优化 持续使用中续期
  const storage = JSON.parse(window[config.type].getItem(key))
  console.log(storage)
  let nowTime = Date.now()
  console.log(config.expire * 6000, nowTime - storage.time)
  // 过期删除
  if (storage.expire && config.expire * 6000 < nowTime - storage.time) {
    removeStorage(key)
    return null
  } else {
    // 未过期期间被调用 则自动续期 进行保活
    setStorage(key, storage.value)
    return storage.value
  }
}

// 名称前自动添加前缀
const autoAddPrefix = (key) => {
  const prefix = config.prefix ? config.prefix + '_' : ''
  return prefix + key
}

// 删除 removeStorage
export const removeStorage = (key) => {
  window[config.type].removeItem(autoAddPrefix(key))
}
