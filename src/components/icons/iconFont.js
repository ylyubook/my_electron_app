/*
 * @Author: ylyu
 * @Date: 2024-02-04 12:05:29
 * @LastEditors: ylyu
 * @LastEditTime: 2024-02-23 15:03:52
 * @Description:
 */
import { h } from 'vue'
import { Icon } from '@arco-design/web-vue'

const IconFont = Icon.addFromIconFontCn({
  // src: 'https://at.alicdn.com/t/c/font_4136370_kdxdahjtbci.js',
  src: 'https://at.alicdn.com/t/c/font_4440212_1c55zhd28a2.js',
  extraProps: {
    type: 'icon-home',
    style: {
      fontSize: '24px',
      // color: '#c9cdd4',
      // color: '#ffffff',
    },
  },
})
const DynamicIconFont = (props) => {
  return h(IconFont, { type: props.type || 'icon-kuangjia' })
}

export default DynamicIconFont
