/*
 * @Author: ylyu
 * @Date: 2024-02-04 12:05:29
 * @LastEditors: ylyu
 * @LastEditTime: 2024-02-21 17:11:42
 * @Description:
 */
import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import ArcoVue from '@arco-design/web-vue'
import ArcoVueIcon from '@arco-design/web-vue/es/icon'
import '@arco-design/web-vue/dist/arco.less'
import Router from './router/index'
import components from './components/icons'

//引入
const app = createApp(App)
// components
for (const i in components) {
  app.component(i, components[i])
}
app.use(ArcoVue).use(ArcoVueIcon).use(Router).mount('#app')
