/*
 * @Author: ylyu
 * @Date: 2024-02-22 12:24:39
 * @LastEditors: ylyu
 * @LastEditTime: 2024-02-22 17:39:05
 * @Description:
 */
// renderer.js - Electron渲染进程代码
const { ipcRenderer } = require('electron')
// const { AppPost } = require('@/utils/request')
// const { checkLogin } = require('@/services')
// import axios from 'axios'

const submitForm = (e) => {
  console.log(e)
  // e.preventDefault() // 阻止默认提交事件
  const usernameInput = document.querySelector('[name=username]').value
  const passwordInput = document.querySelector('[name=password]').value
  const rememberMe = document.querySelector('[name=rememberMe]').checked
  if (!usernameInput || !passwordInput) {
    return
  }
  ipcRenderer.send('login', usernameInput, passwordInput, rememberMe.toString()) // 向主进程发送登录信息
}

window.addEventListener('DOMContentLoaded', () => {
  const form = document.getElementById('loginForm')
  form.addEventListener('submit', submitForm)
  const submitBtn = document.getElementById('submitBtn')
  submitBtn.addEventListener('click', () => {
    console.log('submit!!!!')
    submitForm()
  })
})

ipcRenderer.on('loggedIn', (options) => {
  console.log('Successfully logged in!', options)
})
